<?php

class ${CLASSNAME}
    extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('${RESOURCEMODEL}');
    }

}

<?php

namespace ${Vendor}\\${Module}\\${NAME};

use Magento\Framework\Event;

/**
 * Class ${NAME}
 * @package ${Vendor}\\${Module}\\${NAME}
 */
class ${NAME}
    implements Event\ObserverInterface
{
    /**
     * Observer constructor.
     */
    public function __construct(
        
    ) {
        // Observer initialization code
        // You can use dependency injection to get any class this observer may need
    }

    /**
     * @param Event\Observer ${DS}observer
     */
    public function execute(Event\Observer ${DS}observer)
    {
        // TODO: Implement execute() method.
    }
}

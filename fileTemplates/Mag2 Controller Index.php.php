<?php

namespace ${Vendor}\\${Module}\Controller\\${NAME};

use Magento\Framework\App\Action;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class ${NAME}
 * @package ${Vendor}\\${Module}\Controller\\${NAME}
 */
class ${NAME}
    extends Action\Action
{
    /**
      * @var PageFactory
      */
    protected ${DS}_resultPageFactory;
    
    /**
     * Index constructor.
     * @param Action\Context ${DS}context
     * @param PageFactory    ${DS}resultPageFactory
     */
    public function __construct(
        Action\Context ${DS}context,
        PageFactory ${DS}resultPageFactory
    ) {
        ${DS}this->_resultPageFactory = ${DS}resultPageFactory;
        parent::__construct(${DS}context); 
    }
    
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        ${DS}resultPage = ${DS}this->_resultPageFactory->create();
        
        return ${DS}resultPage;
    }
}

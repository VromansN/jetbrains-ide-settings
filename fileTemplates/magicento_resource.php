<?php

class ${CLASSNAME}
    extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('${TABLE}', '${PK}');
    }

}

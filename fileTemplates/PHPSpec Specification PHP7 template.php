<?php

declare(strict_types=1);

#if (${NAMESPACE} != '')
namespace spec\\${NAMESPACE};
#end

#if (${NAMESPACE} && ${CLASS_TO_TEST})
use ${NAMESPACE}\\${CLASS_TO_TEST};
#elseif (!${NAMESPACE} && ${CLASS_TO_TEST})
use ${CLASS_TO_TEST};
#end
use PhpSpec\ObjectBehavior;

class ${NAME} extends ObjectBehavior
{
    public function let(
        
    ): void {
        ${DS}this->beConstructedWith();
    }
    
    public function it_is_initializable(): void
    {
        ${DS}this->shouldHaveType(#if (${CLASS_TO_TEST}) ${CLASS_TO_TEST}::class #end);
    }
    
    public function it_should_contain_at_least_one_decent_test(): void
    {
        
    }
}

<?php

namespace ${Vendor}\\${Module}\Block;

use Magento\Framework\View\Element\Template;

/**
 * Class ${NAME}
 * @package ${Vendor}\\${Module}\Block
 */
class ${NAME}
    extends Template 
{
    /**
     * ${NAME} constructor. (update according to needs)
     * @param Templage\Context ${DS}context
     * @param array            ${DS}data
     */
    public function __construct(
        Template\Context ${DS}context,
        array ${DS}data = []
    ) {
        parent::__construct(${DS}context, ${DS}data);
    }

    /**
     * This is an example, replace/remove/alter according to needs
     *
     * @param string ${DS}key
     * @param null   ${DS}index
     * @return mixed
     */
    public function getData(
        ${DS}key = '',
        ${DS}index = null
    ) {
        // code here
        return parent::getData(${DS}key, ${DS}index);
    }
}

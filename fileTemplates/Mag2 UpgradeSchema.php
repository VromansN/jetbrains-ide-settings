<?php

namespace ${Vendor}\\${Module}\Setup;

use Magento\Framework\Setup;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class ${NAME}
 * @package ${Vendor}\\${Module}\Setup
 */
class ${NAME}
    implements Setup\UpgradeSchemaInterface
{
    /**
     * @param Setup\SchemaSetupInterface    ${DS}setup
     * @param Setup\ModuleContextInterface  ${DS}context
     */
    public function upgrade(
        Setup\SchemaSetupInterface ${DS}setup,
        Setup\ModuleContextInterface ${DS}context
    ) {
        ${DS}installer = ${DS}setup;
        ${DS}installer->startSetup();

        if (version_compare(${DS}context->getVersion(), '${Version}', '<')) {
            ${DS}connection = ${DS}installer->getConnection();
            
            // replace code below with what you need
            ${DS}column = [
                'type' => Table::TYPE_SMALLINT,
                'length' => 6,
                'nullable' => false,
                'comment' => 'Comment',
                'default' => '1'
            ];
            
            ${DS}connection->addCollumn(
                ${DS}setup->getTable('table_name'),
                'column_name',
                ${DS}column
            );
        }

        ${DS}installer->endSetup();
    }
}

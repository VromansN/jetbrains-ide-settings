<?php

namespace ${Vendor}\\${Module}\Setup;

use Magento\Framework\Setup;

/**
 * Class ${NAME}
 * @package ${Vendor}\\${Vendor}\Setup
 */
class ${NAME}
    implements Setup\InstallDataInterface
{
    /**
     * @param Setup\ModuleDataSetupInterface    ${DS}setup
     * @param Setup\ModuleContextInterface      ${DS}context
     */
    public function install(
        Setup\ModuleDataSetupSetupInterface ${DS}setup,
        Setup\ModuleContextInterface ${DS}context
    ) {
        // replace below code with what you need
        ${DS}data = [
            'title' => 'Title',
            'is_active' => 1
        ]; 
        
        // ${DS}this->setData($data)->save();
    }
}

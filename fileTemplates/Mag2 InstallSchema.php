<?php

namespace ${Vendor}\\${Module}\Setup;

use Magento\Framework\Setup;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;

/**
 * Class ${NAME}
 * @package ${Vendor}\\${Vendor}\Setup
 */
class ${NAME}
    implements Setup\InstallSchemaInterface
{
    /**
     * @param Setup\SchemaSetupInterface    ${DS}setup
     * @param Setup\ModuleContextInterface  ${DS}context
     */
    public function install(
        Setup\SchemaSetupInterface ${DS}setup,
        Setup\ModuleContextInterface ${DS}context
    ) {
        ${DS}installer = ${DS}setup;
        ${DS}installer->startSetup();

        // replace below code with what you need
        ${DS}table = ${DS}installer
            ->getConnection()
            ->newTable(
                ${DS}installer->getTable('table_name')
            )->addColumn(
                'id',
                Table::TYPE_SMALLINT,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'ID (comment)'
            )->addColumn(
                'column_name',
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Column Comment'
            )->addColumn(
                'column_timestamp',
                Table::TYPE_TIMESTAMP,
                null,
                [],
                'Timestamp Comment'
            )->addIndex(
                ${DS}setup-etIdxName(
                    ${DS}installer->getTable('table_name'),
                    ['index'],
                    ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
                ),
                ['index'],
                ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
            )->setComment('Table Comment');

        ${DS}installer->getConnection()->createTable(${DS}table);

        ${DS}installer->endSetup();
    }
}
